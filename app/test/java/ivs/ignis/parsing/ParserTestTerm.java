package ivs.ignis.parsing;

import ivs.ignis.tokenizing.Token;
import ivs.ignis.tokenizing.TokenType;
import org.apfloat.Apfloat;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Vojtěch Bargl <bargl.vojtech@gmail.com>
 */
public class ParserTestTerm extends ParserTestSuite {

    public static Token MULTIPLY = new Token(TokenType.MULTIPLY, "*");

    public static Token DIVIDE = new Token(TokenType.DIVIDE, "/");

    public static Token INT_2 = new Token(TokenType.INTEGER, "2");

    public static Token INT_3 = new Token(TokenType.INTEGER, "3");

    public static Token INT_6 = new Token(TokenType.INTEGER, "6");

    public static Token INT_7 = new Token(TokenType.INTEGER, "7");

    @Test
    public void termOnly() throws Exception {
        tokenizer(INT_42);
        Assert.assertEquals(new Apfloat("42"), parser.parseTerm());
    }

    @Test
    public void simpleMul() throws Exception {
        tokenizer(INT_6, MULTIPLY, INT_7);
        Assert.assertEquals(new Apfloat("42"), parser.parseTerm());
    }

    @Test
    public void simpleDiv() throws Exception {
        tokenizer(INT_42, DIVIDE, INT_6);
        Assert.assertEquals(new Apfloat("7"), parser.parseTerm());
    }

    @Test
    public void complexMul() throws Exception {
        tokenizer(INT_2, MULTIPLY, INT_3, MULTIPLY, INT_7);
        Assert.assertEquals(new Apfloat("42"), parser.parseTerm());
    }

    @Test
    public void complexDiv() throws Exception {
        tokenizer(INT_42, DIVIDE, INT_7, DIVIDE, INT_3);
        Assert.assertEquals(new Apfloat("2"), parser.parseTerm());
    }
}