package ivs.ignis.tokenizing;

import java.util.*;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 * @author Vojtěch Bargl <bargl.vojtech@gmail.com>
 */
public class Tokenizer {

    private static HashMap<String, Token> knownCharacters = new HashMap<>();

    private static Token EOF = new Token(TokenType.EOF, null);

    static {
        knownCharacters.put("/", new Token(TokenType.DIVIDE, "/"));
        knownCharacters.put("*", new Token(TokenType.MULTIPLY, "*"));
        knownCharacters.put("+", new Token(TokenType.PLUS, "+"));
        knownCharacters.put("-", new Token(TokenType.MINUS, "-"));
        knownCharacters.put("(", new Token(TokenType.OPEN_PARENTHESIS, "("));
        knownCharacters.put("^", new Token(TokenType.POWER, "^"));
        knownCharacters.put(")", new Token(TokenType.CLOSED_PARENTHESIS, ")"));
        knownCharacters.put("!", new Token(TokenType.FACTORIAL, "!"));
    }

    private InputTextReader input;

    private List<Token> tokens;

    private Token peek = null;

    public Tokenizer() {
        this.tokens = new LinkedList<>();
        this.reset("");
    }

    /**
     * Initialize tokenizer for new tokenizing.
     *
     * @param text Text to be tokenized
     */
    public void reset(String text) {
        this.tokens.clear();
        this.peek = null;
        this.input = new InputTextReader(text);
        this.tokenizeSingleToken();
    }

    /**
     * Tokenize single token and get current.
     *
     * @return Current token
     */
    public Token gain() {
        this.tokenizeSingleToken();

        return this.previous();
    }

    /**
     * Gain token and check its type, if not equal throw UnrecognizedSequenceException
     *
     * @param expected - expected type of token
     *
     * @return Stepped token
     */
    public Token step(TokenType expected) {
        Token token = gain();
        if (token.getType() != expected) throw new UnrecognizedSequenceException();

        return token;
    }

    /**
     * @return Next token.
     */
    public Token peek() {
        if (this.peek != null) return this.peek;

        return this.reachedEnd() ? null : (this.peek = this.getToken());
    }

    /**
     * @return Current token.
     */
    public Token current() {
        return this.tokens.get(this.tokens.size() - 1);
    }

    /**
     * @return Previous token.
     */
    public Token previous() {
        return (this.tokens.isEmpty() || this.tokens.size() == 1) ? null : this.tokens.get(this.tokens.size() - 2);
    }

    /**
     * @return All tokens.
     */
    public List<Token> previousTokens() {
        if (this.tokens.isEmpty() || this.tokens.size() == 1) return Collections.emptyList();
        return new ArrayList<>(this.tokens.subList(0, this.tokens.size() - 2));
    }

    /**
     * Transform input into list of tokens.
     *
     * @return All tokens from current position
     */
    public List<Token> tokenize() {
        while (!this.reachedEnd()) {
            tokenizeSingleToken();
        }

        return new ArrayList<>(tokens.subList(0, tokens.size()));
    }

    /**
     * Tokenize following text if any.
     */
    public void tokenizeSingleToken() {
        if (this.reachedEnd()) return;

        this.tokens.add((this.peek != null) ? this.peek : this.getToken());
        this.peek = null;
    }

    /**
     * @return True if tokenizer reached end, False otherwise
     */
    public boolean reachedEnd() {
        return !this.tokens.isEmpty() && this.tokens.get(this.tokens.size() - 1).isType(TokenType.EOF);
    }

    /**
     * Get lastOne token in input.
     *
     * @return Next token from current position
     */
    private Token getToken() {

        if (!this.input.hasNext()) return EOF;

        if (Character.isWhitespace(this.input.peek())) this.skipWhitespaces();

        if (Character.isDigit(this.input.peek())) return this.tokenizeNumber();

        Token token = knownCharacters.get(String.valueOf(this.input.peek()));
        if (token != null) {
            input.next();
            return token;
        }

        throw new UnrecognizedSequenceException();
    }

    /**
     * Tokenize number in input.
     *
     * @return Number token. Can be double or integer.
     */
    private Token tokenizeNumber() {
        String integer = tokenizeDigitSequence();

        if (!this.input.hasNext() || this.input.peek() != '.') {
            return new Token(TokenType.INTEGER, integer);
        } else {
            this.input.next();
            return new Token(TokenType.DOUBLE, integer + '.' + tokenizeDigitSequence());
        }
    }

    /**
     * Tokenize digit sequence in input.
     *
     * @return Digit sequence.
     */
    private String tokenizeDigitSequence() {
        StringBuilder sb = new StringBuilder();

        do {
            sb.append(this.input.next());
        } while (this.input.hasNext() && Character.isDigit(this.input.peek()));

        return sb.toString();
    }

    /**
     * Skip whitespaces in input.
     */
    private void skipWhitespaces() {
        while (this.input.hasNext() && Character.isWhitespace(this.input.peek())) {
            this.input.next();
        }
    }
}
