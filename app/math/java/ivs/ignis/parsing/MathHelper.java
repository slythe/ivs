package ivs.ignis.parsing;

import org.apfloat.Apfloat;

/**
 * @author Vojtěch Bargl <bargl.vojtech@gmail.com>
 */
class MathHelper {

    private static Apfloat ZERO = Apfloat.ZERO;

    private static Apfloat ONE  = Apfloat.ONE;

    private MathHelper() {}

    /**
     * F(n) = n * F(n - 1).
     */
    public static Apfloat factorial(Apfloat number, long precision) {
        if (number.compareTo(ZERO) < 0) { throw new IllegalArgumentException("Factorial of negative number!"); }

        Apfloat first = ONE;
        for (Apfloat i = ONE; number.compareTo(i) >= 0; i = i.add(ONE)) {
            first = first.multiply(i).precision(precision);
        }

        return first;
    }

    /**
     * F(n+2) = F(n+1) + F(n).
     */
    public static Apfloat fibonacci(Apfloat number, long precision) {
        Apfloat res  = ZERO;
        Apfloat tmp1 = ONE;

        for (Apfloat i = ONE; number.compareTo(i) >= 0; i = i.add(ONE)) {
            Apfloat tmp = res.add(tmp1).precision(precision);
            res = tmp1;
            tmp1 = tmp;
        }

        return res;
    }
}
