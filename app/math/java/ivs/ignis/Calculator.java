package ivs.ignis;

import ivs.ignis.parsing.Parser;
import ivs.ignis.tokenizing.Tokenizer;
import org.apfloat.Apfloat;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
public class Calculator {

    private static Calculator ourInstance = new Calculator();

    private Parser parser;

    private Tokenizer tokenizer;

    private Calculator() {
        this.tokenizer = new Tokenizer();
        this.parser = new Parser(tokenizer);
    }

    public static Calculator getInstance() {
        return ourInstance;
    }

    /**
     * @param statement Statement to calculate
     *
     * @return Statement result
     */
    public Apfloat calculate(String statement) {
        tokenizer.reset(statement);

        return this.parser.parse();
    }
}
