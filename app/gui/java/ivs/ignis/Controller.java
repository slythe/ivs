package ivs.ignis;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.apfloat.Apfloat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;
import java.util.stream.IntStream;

/** @author Jakub "Ash258" Čábera <cabera.jakub@gmail.com> */
public class Controller {

    private static HashMap<String, String> buttons = new HashMap<>();

    private static HashMap<String, String> operands = new HashMap<>();

    static {
        //region Base
        IntStream.range(0, 10).forEach(number -> buttons.put(String.valueOf(number), String.valueOf(number)));
        buttons.put(".", ".");
        //endregion

        //region Operators
        Arrays.asList("+", "-", "*").forEach(symbol -> operands.put(symbol, symbol));
        operands.put("÷", "/");
        operands.put("n!", "!");
        //endregion
    }

    @FXML
    public Pane topPane, inputPane, extendedFunctionsPane, baseButtonsPane;

    @FXML
    public TextField inputField;

    @FXML
    public Label inputTop;

    private HashMap<String, Runnable> special = new HashMap<>();

    private Stack<String> history = new Stack<>();

    {
        special.put("CE", this::CE);
        special.put("CP", this::CP);
        special.put("CA", this::CA);
        special.put("BACKSPACE", this::backspace);
        special.put("±", this::negate);
    }

    /** Clear only input field. */
    @FXML
    public void CE() { inputField.clear(); }

    /** Clear previous input field. */
    @FXML
    public void CP() {
        history.pop();
        inputTop.setText(printStack(history));
    }

    /** Clear history and input field. */
    @FXML
    public void CA() {
        inputField.clear();
        inputTop.setText("");
        history.clear();
    }

    /** Remove last character from input field. */
    @FXML
    public void backspace() {
        String input = inputField.getText();
        if (input.length() != 0) inputField.deleteText(input.length() - 1, input.length());
    }

    @FXML
    public void negate() { inputField.insertText(0, "-"); }

    /**
     * Print given stack in suitable format.
     *
     * @param stack Stack for printing.
     *
     * @return Formatted string for calculation.
     */
    private String printStack(Stack<String> stack) {
        StringBuilder result = new StringBuilder();
        stack.forEach(result::append);
        return result.toString();
    }

    @FXML
    // FEATURE: Add behaviour like win calculator (Multiple press should increase everytime by last known value)
    // FEATURE: After evaluating, next character (except "=") should delete whole input.
    // FEATURE: Handle too long results.
    public void evaluate() {
        String expression = inputTop.getText() + inputField.getText();
        inputTop.setText("");
        try {
            Apfloat result = Calculator.getInstance().calculate(expression);
            inputField.setText(result.toString(true));
            history.clear();
        } catch (Exception e) {
            inputField.setText(e.getMessage());
        }
    }

    @FXML
    // FEATURE: Auto add space or comma after thousand.
    // FEATURE: Add continuous evaluate (Windows like)
    public void processInput(MouseEvent mouseEvent) {
        String valueOfButton = ((Button) mouseEvent.getSource()).getText();
        if (buttons.containsKey(valueOfButton)) {
            inputField.appendText(buttons.get(valueOfButton));
        } else if (operands.containsKey(valueOfButton)) {
            history.add(inputField.getText()); // Add input field to history
            history.add(operands.get(valueOfButton));
            inputTop.setText(printStack(history));
            inputField.clear();
        } else if (special.containsKey(valueOfButton)) {
            special.get(valueOfButton).run();
        }
    }
}
